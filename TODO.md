### TODO
- [Refactor]: Clean up bson.c
- [Streamline]: Replace assertions
- [Refactor]: Create function to set token properties
- [Edge case]: Access illegal memory in tokenizer, assertion present for now.
### Current state
- With proper file
  - No memory leaks
  - No (discovered) crashes
- Fuzzed files
  - No discovered issues
