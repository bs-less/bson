# Bs-less Object Notation
## General purpose substitute of JSON.
### Description
Bson is not intended as the exact replacement of JSON. This project aims at simplifying JSON while having some of the same features that JSON provides. The application goal is intented to be used with standalone applications and not at all intented for web development or anything IoT. This build currently follows specification BSON23.
### Concerns
- BSON already exists! It binary encoded javascript object notation!

Yeah well that acronym is pretty BS and I don't care.
- The absense of colons make it harder to read and the syntax ambiguous!

Colons are optional, you can align with whitespace, and no, the syntax is not ambiguous.
### Install
Dependencies
- Valid C compiler (Tested with GCC or CLANG)
- Compile under C11
Commands
```bash
$ git clone https://gitea.com/bs-less/bson.git
$ cd bson
$ make
$ make install
```
Installing will `cp libbson.so /usr/local/lib/` and `cp -r include /usr/local/include/bson`. (note include renamed to directory `bson`)
### Syntax
```
// This is a comment.

// Below is an integer
three 3

// Below is a floating point
pi 3.14159

// Below is a string
name "John Doe"

// Below is an array, any type can be stored in the array
array [ 1 2 3 ]

// Below is an object. Just like an array but a key must still be associated with it.
object {
    // Simple data
    number   6
    decimal 12.3
    string  "Hello World!"
    // These are the only enabled builtin's by default
    // Builtins can be added, refer to documentation
    running  $true
    swimming $false
    flying   $void  // This key will not exist
    // Nested arrays and objects
    arr [ 1.1  "Jane"  $true ]
    nest {
        message "Hello Object!"
    }
}

```
Currently upon any misc error libbson will attempt to free whatever data it may have already read.
### Notes
Documentation is not yet available, currently being written.\
Specification is not yet available, currently being written.

December 4th, 2023
